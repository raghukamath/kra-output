#!/bin/bash
# kra processing
# Wrote this script by taking some inspiration from bloodywing and David revoy's script

#: Author      : Raghavendra kamath < raghu@raghukamath.com >
#: License     : GPL


# get current working directory
export cwd="$PWD"

#create appropriate folders for output
mkdir -p output/{hires,lowres}

#create a folder in tmp for unzipping contents of kra file
mkdir -p /tmp/kra-output

#start the loop for each file in cwd
for f in *.kra
do
echo "processing file - $f"
#unzip the merged png image inside kra file
unzip -j "$f" "mergedimage.png" -d "/tmp/kra-output/" > /dev/null 2>&1

#convert the extracted png into normal srgb file and copy it back to output folder
#note that -colorspace RGB is used before resize, other wise the resultant jpg is gray-scaled in recent version of imagemagick
#check wether user has given resize parameter as an argument

if [ "$1" != "" ]; then
	convert /tmp/kra-output/mergedimage.png -background white -alpha remove -colorspace RGB -resize "$1"% -unsharp 0.5x0.5+0.5+0.010 -colorspace sRGB -quality 95 "$cwd"/output/lowres/"${f%.*}"-lowres.jpg
#else use default 40% resize value
else
	convert /tmp/kra-output/mergedimage.png -background white -alpha remove -colorspace RGB -resize 40% -unsharp 0.5x0.5+0.5+0.010 -colorspace sRGB -quality 95 "$cwd"/output/lowres/"${f%.*}"-lowres.jpg
fi

convert /tmp/kra-output/mergedimage.png -colorspace sRGB -background white -alpha remove "$cwd"/output/hires/"${f%.*}"-hires.png

#remove the output image (need to find better way to do this)
rm /tmp/kra-output/mergedimage.png
done

#delete the tmp folder
rm -rf /tmp/kra-output
echo "finished exporting"
