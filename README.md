# kra-output
a small personal bash script to export kra files into lowres jpg

## usage
Add this script to your appropriate bin folder(which is included in your PATH environment variable) and make it executable
Open the terminal in directory containing the kra files and run the following.

kra-output.sh 'resize value'

you can provide a resize value while running the script eg kra-output.sh 30 for resizing the lowres file to 30% 
if no value is provided it will resize it to 40% by default.
